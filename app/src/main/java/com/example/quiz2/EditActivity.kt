package com.example.quiz2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        init()
    }

    private fun init(){
        var intentt=intent
        var intent=intent.extras!!
        firstname.setText(intent.getString("firstname"))
        lastname.setText(intent.getString("lastname"))
        Email.setText(intent.getString("email"))

        savebtn.setOnClickListener(){
            intentt.putExtra("newname",firstname.text.toString())
            intentt.putExtra("newlastname",lastname.text.toString())
            intentt.putExtra("newemail",Email.text.toString())
            setResult(Activity.RESULT_OK,intentt)
            finish()
        }
    }
}

package com.example.quiz2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycle_view.view.*

class MyAdapter(val ContentList:MutableList<Mymodel>,private val pos: deleteinterface,private val posi: editinterface):RecyclerView.Adapter<MyAdapter.viewHolder>()  {

    override fun getItemCount()=ContentList.size



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        return viewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycle_view,parent,false))
    }



    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        holder.onbind()
    }

    inner  class viewHolder(view: View): RecyclerView.ViewHolder(view){
        lateinit var  model:Mymodel
        fun onbind(){
            model=ContentList[adapterPosition]
            itemView.firstname.text=model.firstname
            itemView.lastname.text=model.lastname
            itemView.Email.text=model.email

            itemView.deletebtn.setOnClickListener(){
                pos.delete(adapterPosition)
            }
            itemView.editbtn.setOnClickListener(){
                posi.edit(adapterPosition)
            }
  }


    }
}
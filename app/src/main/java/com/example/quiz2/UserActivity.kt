package com.example.quiz2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_main.*

class UserActivity : AppCompatActivity() {
    var pos:Int=0
    var REQUEST_CODE=1
    var ContentList = mutableListOf<Mymodel>()
    private lateinit var adapter:MyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }


    private fun init(){

        adapter= MyAdapter(ContentList,object: deleteinterface{
            override fun delete(position: Int) {
                ContentList.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

        },object: editinterface{
            override fun edit(position: Int) {
               editactivity(position)
            }

        })
        mainLayout.layoutManager= LinearLayoutManager(this)
        mainLayout.adapter=adapter

        addbtn.setOnClickListener(){

            setdate()
            adapter.notifyItemInserted(0)
            mainLayout.scrollToPosition(0)

        }
    }

    private fun editactivity(position:Int){
        pos=position
        intent= Intent(this,EditActivity::class.java)
        intent.putExtra("firstname",ContentList[position].firstname)
        intent.putExtra("lastname",ContentList[position].lastname)
        intent.putExtra("email",ContentList[position].email)
        startActivityForResult(intent,REQUEST_CODE)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==REQUEST_CODE && resultCode== Activity.RESULT_OK){

            var inte=data!!.extras
            var model=ContentList[pos]
            model.firstname=inte!!.getString("newname","")
            model.lastname=inte!!.getString("newlastname","")
            model.email=inte.getString("newemail","")


            adapter.notifyDataSetChanged()


            Log.d("change", ContentList[pos].firstname)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private  fun setdate(){
        val randomContext= mutableListOf<Mymodel>()

        var model1=Mymodel(" Robert","De Niro","rdeniro@gmail.com")
        val model2=Mymodel( "Jack","Nicholson","Nicholson@gmail.com")
        val model3=Mymodel(  "marlon","Brando","Brando@gmail.com")
        val model4=Mymodel( "Leonardo","DiCaprio","DiCaprio@gmail.com" )


        randomContext.add(model1)
        randomContext.add(model2)
        randomContext.add(model3)
        randomContext.add(model4)



        ContentList.add(0,randomContext[(randomContext.indices).random()])


    }

}
